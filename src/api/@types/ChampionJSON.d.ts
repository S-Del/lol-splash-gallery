declare interface Skin {
    id: string;
    num: number;
    name: string;
    chromas: boolean;
}

declare interface Info {
    attack: number;
    defense: number;
    magic: number;
    difficulty: number;
}

declare interface Stats {
    hp: number;
    hpperlevel: number;
    mp: number;
    mpperlevel: number;
    movespeed: number;
    armor: number;
    armorperlevel: number;
    spellblock: number;
    spellblockperlevel: number;
    attackrange: number;
    hpregen: number;
    hpregenperlevel: number;
    mpregen: number;
    mpregenperlevel: number;
    crit: number;
    critperlevel: number;
    attackdamage: number;
    attackdamageperlevel: number;
    attackspeedperlevel: number;
    attackspeed: number;
}

declare interface Leveltip {
    label: string[];
    effect: string[];
}

declare interface Image {
    full: string;
    sprite: string;
    group: string;
    x: number;
    y: number;
    w: number;
    h: number;
}

declare interface Spell {
    id: string;
    name: string;
    description: string;
    tooltip: string;
    leveltip: Leveltip;
    maxrank: number;
    cooldown: number[];
    cooldownBurn: string;
    cost: number[];
    costBurn: string;
    datavalues: Datavalues;
    effect: number[][];
    effectBurn: string[];
    vars: any[];
    costType: string;
    maxammo: string;
    range: number[];
    rangeBurn: string;
    image: Image;
    resource: string
}

declare interface Passive {
    name: string;
    description: string;
    image: Image;
}

declare interface Item {
    id: string;
    count: number;
    hideCount?: boolean;
}

declare interface Block {
    type: string;
    maxSummonerLevel: number;
    items: Item[];
    minSummonerLevel?: number;
    recMath?: boolean;
    recSteps?: boolean;
    showIfSummonerSpell: string;
    hideIfSummonerSpell: string;
    appendAfterSection: string;
    visibleWithAllOf: string[];
    hiddenWithAnyOf: string[];
}

declare interface Recommended {
    champion: string;
    title: string;
    type: string;
    map: string;
    priority: boolean;
    blocks: Block[];
    customTag: string;
    sortrank?: number;
    extensionPage?: boolean;
    customPanel?: any;
    useObviousCheckmark?: boolean;
}

declare interface ChampionData {
    version?: string;
    id: string;
    key: string;
    name: string;
    title: string;
    image?: Image;
    skins?: Skin[];
    lore?: string;
    blurb: string;
    allytips?: string[];
    enemytips?: string[];
    tags: string[];
    partype: string;
    info?: Info;
    stats?: Stats;
    spells?: Spell[];
    passive?: Passive;
    recommended?: Recommended[];
}

declare interface ChampionsData {
    [formalName: string]: ChampionData;
}

declare interface ChampionJSON {
    type: string;
    format: string;
    version: string;
    data: ChampionsData;
}
