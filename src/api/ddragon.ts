import axios, { AxiosResponse } from 'axios';

const base = 'https://ddragon.leagueoflegends.com';

export const fetchVersions = (): Promise<AxiosResponse<string[]>> => {
    return axios.get<string[]>(`${base}/api/versions.json`);
};

export const fetchLanguages = (): Promise<AxiosResponse<string[]>> => {
    return axios.get<string[]>(`${base}/cdn/languages.json`);
};

export const fetchAllChampionsData = (
    version: string,
    language: string,
    verbose = false
): Promise<AxiosResponse<ChampionJSON>> => {
    return axios.get<ChampionJSON>([
        `${base}/cdn/${version}/data/${language}`,
        `/${verbose ? 'championFull':'champion'}.json`
    ].join(''));
};

export const fetchChampionData = (
    champName: string,
    version: string,
    language: string
): Promise<AxiosResponse<ChampionJSON>> => {
    return axios.get<ChampionJSON>([
        `${base}/cdn/${version}/data/${language}/champion/${champName}.json`,
    ].join(''));
};
