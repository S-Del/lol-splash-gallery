import { fetchVersions } from './api/ddragon';
import { retrieveElement } from './common';
import { changeOptionEvent } from './event';
import {
    updateLanguageOptions,
    updateChampionOptions,
    updateSplashOptions
} from './options';

void (async () => {
    const latest = (await fetchVersions()).data[0]

    const header = retrieveElement<HTMLHeadingElement>('header');
    const languageSelect = retrieveElement<HTMLSelectElement>(
        '#language_select', header
    );
    await updateLanguageOptions(languageSelect);

    const championSelect = retrieveElement<HTMLSelectElement>(
        '#champion_select', header
    );
    await updateChampionOptions(championSelect, latest, languageSelect.value);

    const splashSelect = retrieveElement<HTMLSelectElement>(
        '#splash_select', header
    );
    const imgElement = retrieveElement<HTMLImageElement>('#splash_image');
    await updateSplashOptions(
        splashSelect,
        imgElement,
        latest,
        languageSelect.value,
        championSelect.value
    );

    changeOptionEvent(
        languageSelect,
        championSelect,
        splashSelect,
        imgElement,
        latest
    );
})();
