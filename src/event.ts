import {
    updateChampionOptions,
    updateSplashOptions
} from './options';

export const changeOptionEvent = (
    languageSelect: HTMLSelectElement,
    championSelect: HTMLSelectElement,
    splashSelect: HTMLSelectElement,
    imgElement: HTMLImageElement,
    version: string
): void => {
    languageSelect.addEventListener('change', () => {
        void (async ()=>{
            const newLanguage = languageSelect.value;
            await updateChampionOptions(championSelect, version, newLanguage);
            const championName = championSelect.value;
            await updateSplashOptions(
                splashSelect,
                imgElement,
                version,
                newLanguage,
                championName
            );
        })();
    });

    championSelect.addEventListener('change', () => {
        void (async () => {
            const language = languageSelect.value;
            const newChampion = championSelect.value;
            await updateSplashOptions(
                splashSelect,
                imgElement,
                version,
                language,
                newChampion
            );
        })();
    });

    splashSelect.addEventListener('change', () => {
        const championName = championSelect.value;
        const num = splashSelect.value;
        imgElement.src = [
            'https://ddragon.leagueoflegends.com',
            `/cdn/img/champion/splash/${championName}_${num}.jpg`
        ].join('');
    });
};
