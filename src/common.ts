export const retrieveElement = <T extends HTMLElement>(
    selector: string,
    parent?: HTMLElement
): T => {
    let element = null;
    if (!parent) {
        element = document.querySelector<T>(selector);
    } else {
        element = parent.querySelector<T>(selector);
    }

    if (!element) throw new Error();
    return element;
};

export const removeAllChild = (parent: HTMLElement): void => {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
};
