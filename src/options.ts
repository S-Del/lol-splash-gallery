import {
    fetchAllChampionsData,
    fetchChampionData,
    fetchLanguages
} from './api/ddragon';
import { removeAllChild } from './common';

export const updateLanguageOptions = async (
    select: HTMLSelectElement
): Promise<void> => {
    removeAllChild(select);
    const resp = await fetchLanguages();
    for (const lang of resp.data) {
        const option = document.createElement('option');
        option.value = lang;
        option.textContent = lang;
        select.appendChild(option);
    }
};

export const updateChampionOptions = async (
    select: HTMLSelectElement,
    version: string,
    language: string
): Promise<void> => {
    removeAllChild(select);
    const data = (
        await fetchAllChampionsData(version, language)
    ).data.data;
    for (const key in data) {
        const option = document.createElement('option');
        option.value = key;
        option.textContent = data[key].name;
        select.appendChild(option);
    }
};

export const updateSplashOptions = async (
    select: HTMLSelectElement,
    imgElement: HTMLImageElement,
    version: string,
    language: string,
    championName: string
): Promise<void> => {
    removeAllChild(select);
    const champData = (
        await fetchChampionData(championName, version, language)
    ).data.data[championName];
    if (!champData.skins) throw new Error();

    for (const skin of champData.skins) {
        const option = document.createElement('option');
        option.value = String(skin.num);
        option.textContent = skin.name;
        select.appendChild(option);
    }

    const num = select.value;
    imgElement.src = [
        'https://ddragon.leagueoflegends.com',
        `/cdn/img/champion/splash/${championName}_${num}.jpg`
    ].join('');
};
